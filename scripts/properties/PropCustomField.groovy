package properties

import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import properties.PropEnvironment

enum PropCustomField {
    TASK_MODE(["PROD": 00000, "DEV": 10209]),
    BASELINE_START_DATE(["PROD": 00000, "DEV": 10207]),
    BASELINE_START_DATE_NEW(["PROD": 00000, "DEV": 12321]),
    BUSINESS_LEADERS(["PROD": 00000, "DEV": 11508])

    PropEnvironment propEnvironment = new PropEnvironment()
    static public CustomFieldManager customFieldManager
    CustomField object
    Long id
    String idString
    PropCustomField(Map<String, Integer> properties) {
        if (customFieldManager == null) customFieldManager = ComponentAccessor.getCustomFieldManager()
        //object = customFieldManager.getCustomFieldObject(properties.get(ENV_NAME))
        //opcja 2
        object = customFieldManager.getCustomFieldObject(properties.get(propEnvironment.envName))
        this.id = properties.get(propEnvironment.envName) as Long
        this.idString = properties.get(propEnvironment.envName).toString()
    }
}

/* TEMP
package properties

import com.atlassian.jira.issue.fields.CustomField
import org.apache.log4j.Logger
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager

class Properties {
    protected static Logger log = Logger.getLogger(this)
    //static final String ENV_NAME = System.getenv().get("JIRA_SCRIPT_PROPERTIES")

    //opcja 2
    //static String ENV_NAME
    //opcja3
    //plik z nazwa środowiska
    static final String BASE_URL = ComponentAccessor.getApplicationProperties().getText("jira.baseurl")
    enum EnvType {
        PROD("http://jira-prod.pl"),
        DEV("http://192.168.78.30:8080/jira")
        final String URL
        String envName
        EnvType(String url){
            this.URL = url
            switch (BASE_URL){
                case PROD.URL:
                    envName = "PROD"
                    break
                case DEV.URL:
                    envName = "DEV"
                    break
            }
        }
    }

    enum PropCustomField {
        TASK_MODE(["PROD": 00000, "DEV": 10209]),
        BASELINE_START_DATE(["PROD": 00000, "DEV": 10207]),
        BASELINE_START_DATE_NEW(["PROD": 00000, "DEV": 12321]),
        BUSINESS_LEADERS(["PROD": 00000, "DEV": 11508])

        static public CustomFieldManager customFieldManager
        CustomField object
        Long id
        String idString

        PropCustomField(Map<String, Integer> properties) {
            if (customFieldManager == null) customFieldManager = ComponentAccessor.getCustomFieldManager()
            //object = customFieldManager.getCustomFieldObject(properties.get(ENV_NAME))
            //opcja 2
            object = customFieldManager.getCustomFieldObject(properties.get(EnvType.PROD.envName))
            this.id = properties as Long
            this.idString = properties.toString()
            //this.idStringBehaviour = "customfield_"
        }
    }

    enum IssueType {
        LINK_ORDER([PROD: 123, DEV: 321]),
        ALT_ORDER([PROD: 243, DEV: 432])

        String id

        IssueType(Map<String, Integer> properties) {
            this.id = properties.get(EnvType.PROD.envName).toString()
        }
    }
}

 */