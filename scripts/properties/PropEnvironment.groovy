package properties

import com.atlassian.jira.component.ComponentAccessor

//static final String ENV_NAME = System.getenv().get("JIRA_SCRIPT_PROPERTIES")

//opcja 2
//static String ENV_NAME
//opcja3
//plik z nazwa środowiska
class PropEnvironment {
    static String envName
    static String baseUrl

    PropEnvironment(){
        baseUrl = ComponentAccessor.getApplicationProperties().getText("jira.baseurl")
        switch (baseUrl) {
            case EnvironmentType.PROD.URL:
                envName = "PROD"
                break
            case EnvironmentType.DEV.URL:
                envName = "DEV"
                break
        }
    }

    enum EnvironmentType {
        PROD("http://jira-prod.pl"),
        DEV("http://192.168.78.30:8080/jira")
        final String URL

        EnvironmentType(String url) {
            this.URL = url
        }
    }
}