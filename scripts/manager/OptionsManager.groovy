package manager

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.customfields.option.Option
import com.atlassian.jira.issue.fields.CustomField

class OptionsManager {
    private static
    final com.atlassian.jira.issue.customfields.manager.OptionsManager optionsManager = ComponentAccessor.getOptionsManager()

    @Deprecated
    static Option findOption(String option) {
        return optionsManager.findByOptionValue(option)[0]
    }

    static Option findOptionSpecificField(CustomField customField, String option, Issue issue) {
        return optionsManager.getOptions(customField.getRelevantConfig(issue)).find { it.value == option }
    }
}
