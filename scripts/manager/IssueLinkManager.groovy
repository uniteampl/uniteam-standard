package manager

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.LogManager
import org.apache.log4j.Logger

class IssueLinkManager {
    final static
    private com.atlassian.jira.issue.link.IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()

    private static final Logger log = LogManager.getLogger(IssueLinkManager)

    static linkIssue(Issue issueFrom, Issue issueTo, Long issueLinkId, ApplicationUser applicationUser) {
        issueLinkManager.createIssueLink(issueFrom.getId(), issueTo.getId(), issueLinkId, 0, applicationUser)
    }

    static ArrayList<Issue> getLinkedIssues(Issue issue) {
        return issueLinkManager.getLinkCollectionOverrideSecurity(issue).getAllIssues()
    }

    static List<IssueLink> getInwardLinks(Long issueId) {
        issueLinkManager.getInwardLinks(issueId)
    }

    static List<IssueLink> getOutwardLinks(Long issueId) {
        issueLinkManager.getOutwardLinks(issueId)
    }

    static boolean removeLinkIssue(Issue issue1, Issue issue2, ApplicationUser applicationUser) {
        IssueLink issue1Link = issueLinkManager.getInwardLinks(issue1.id).find { it.sourceId == issue2.id }
        if (!issue1Link) {
            issue1Link = issueLinkManager.getOutwardLinks(issue1.id).find { it.destinationId == issue2.id }
        }

        if (issue1Link) {
            issueLinkManager.removeIssueLink(issue1Link, applicationUser)
            return true
        } else {
            return false
        }
    }
}
