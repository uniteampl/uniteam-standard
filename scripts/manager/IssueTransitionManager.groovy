package manager

import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.workflow.TransitionOptions
import com.atlassian.jira.workflow.TransitionOptions.Builder
import org.apache.log4j.Logger

class IssueTransitionManager {
    final static private IssueService issueService = ComponentAccessor.getIssueService()

    static final public def log = Logger.getLogger("pl.uniteam.jira.IssueTransitionManager")

    static transition(Issue issue, int transitionID, ApplicationUser applicationUser) {
        IssueInputParametersImpl issueInputParameters = new IssueInputParametersImpl()

        TransitionOptions transitionOptions = new Builder().skipValidators().skipConditions().skipPermissions().build()
        IssueService.TransitionValidationResult validationResult =
                issueService.validateTransition(applicationUser, issue.id, transitionID, issueInputParameters, transitionOptions)
        if (validationResult.isValid()) {
            issueService.transition(applicationUser, validationResult)
        } else {
            log.error "#1Validation result NOT OK ERRORS: " + validationResult.getErrorCollection().getErrorMessages()
            log.warn "#2Validation result NOT OK WARNS: " + validationResult.getWarningCollection().getWarnings()
        }
    }

    static transitionWithCondition(Issue issue, int transitionID, ApplicationUser applicationUser) {
        IssueInputParametersImpl issueInputParameters = new IssueInputParametersImpl()

        TransitionOptions transitionOptions = new Builder().skipValidators().skipPermissions().build()
        IssueService.TransitionValidationResult validationResult =
                issueService.validateTransition(applicationUser, issue.id, transitionID, issueInputParameters, transitionOptions)
        if (validationResult.isValid()) {
            issueService.transition(applicationUser, validationResult)
        } else {
            log.error "#1Validation result NOT OK ERRORS: " + validationResult.getErrorCollection().getErrorMessages()
            log.warn "#2Validation result NOT OK WARNS: " + validationResult.getWarningCollection().getWarnings()
        }
    }

    static transitionWithValidator(Issue issue, int transitionID, ApplicationUser applicationUser) {
        IssueInputParametersImpl issueInputParameters = new IssueInputParametersImpl()

        TransitionOptions transitionOptions = new Builder().skipConditions().skipPermissions().build()
        IssueService.TransitionValidationResult validationResult =
                issueService.validateTransition(applicationUser, issue.id, transitionID, issueInputParameters, transitionOptions)
        if (validationResult.isValid()) {
            issueService.transition(applicationUser, validationResult)
        } else {
            log.error "#1Validation result NOT OK ERRORS: " + validationResult.getErrorCollection().getErrorMessages()
            log.warn "#2Validation result NOT OK WARNS: " + validationResult.getWarningCollection().getWarnings()
        }
    }
}