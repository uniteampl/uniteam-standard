package manager

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.query.Query

class SearchManager {
    final static private SearchService searchService =
            ComponentAccessor.getComponent(SearchService)
    final static private JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)

    static ArrayList<Issue> getIssues(String jql, ApplicationUser user) {
        Query query = jqlQueryParser.parseQuery(jql)

        return searchService.search(user, query, PagerFilter.getUnlimitedFilter())?.getResults()?.collect {
            IssueManager.getMutableIssue(it.key)
        }?.findAll { it != null }
    }

    static ArrayList<MutableIssue> getMutableIssues(String jql, ApplicationUser user) {
        Query query = jqlQueryParser.parseQuery(jql)

        return searchService.search(user, query, PagerFilter.getUnlimitedFilter())?.getResults()?.collect {
            IssueManager.getMutableIssue(it.key)
        }?.findAll { it != null }
    }

    static ArrayList<MutableIssue> getMutableIssuesOLD(String jql, ApplicationUser user) {
        Query query = jqlQueryParser.parseQuery(jql)

        return searchService.search(user, query, PagerFilter.getUnlimitedFilter())?.getResults()?.collect {
            IssueManager.getMutableIssue(it.key)
        }
    }
}