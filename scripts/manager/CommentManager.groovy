package manager

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.comments.Comment
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.util.json.JSONObject


class CommentManager {
    final static
    private com.atlassian.jira.issue.comments.CommentManager commentManager = ComponentAccessor.getCommentManager()
    final static private SD_PUBLIC_COMMENT = "sd.public.comment"

    static commentIssue(Issue issue, String commentBody, ApplicationUser applicationUser) {
        commentManager.create(issue, applicationUser, commentBody, false)
    }

    static commentIssue(Issue issue, Comment comment) {
        commentManager.create(issue, comment.authorApplicationUser, comment.body, false)
    }

    static copyAllComments(Issue fromIssue, Issue toIssue) {
        commentManager.getComments(fromIssue).each { comment ->
            commentManager.create(toIssue, comment.authorApplicationUser, comment.updateAuthorApplicationUser, comment.body, null as String, null as Long, comment.created, comment.updated, false)
        }
    }

    static copyLastComments(Issue fromIssue, Issue toIssue) {
        commentManager.getLastComment(fromIssue).findAll() { comment ->
            commentManager.create(toIssue, comment.authorApplicationUser, comment.updateAuthorApplicationUser, comment.body, null as String, null as Long, comment.created, comment.updated, false)
        }
    }

    static commentIssueAsInternal(Issue issue, String commentBody, ApplicationUser applicationUser) {
        Map properties = [(SD_PUBLIC_COMMENT): new JSONObject(["internal": true] as Map)]
        commentManager.create(issue, applicationUser, commentBody, null, null, new Date(), properties, true)
    }
}