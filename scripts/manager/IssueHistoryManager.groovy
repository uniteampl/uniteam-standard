package manager

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.fields.CustomField
import org.apache.log4j.LogManager
import org.apache.log4j.Logger

import java.sql.Timestamp

class IssueHistoryManager {
    final static private ChangeHistoryManager changeHistoryManager = ComponentAccessor.getChangeHistoryManager()

    private static final Logger log = LogManager.getLogger(IssueHistoryManager)

    static boolean isStatusInIssueHistory(Issue issue, String statusId) {
        return changeHistoryManager.getChangeItemsForField(issue, "status").find { it.to == statusId }
    }

    static String getLatestChangedValue(Issue issue, Collection<String> customFieldNames) {
        ArrayList<ChangeItemBean> changeItemBeanList = new ArrayList<>()
        customFieldNames.each { customFieldName ->
            changeItemBeanList.addAll(changeHistoryManager.getChangeItemsForField(issue, customFieldName))
        }

        Timestamp changeDate = null
        String latestValue = null
        changeItemBeanList.each { changeItemBean ->
            if (!changeDate || changeItemBean.created.after(changeDate)) {
                changeDate = changeItemBean.created
                latestValue = changeItemBean.toString
            }
        }

        return latestValue
    }

    static Timestamp getTimeOfLastEditionOfField(Issue issue, CustomField customField) {
        List<ChangeItemBean> changeItems = changeHistoryManager.getChangeItemsForField(issue, customField.fieldName)
        ChangeItemBean latest = null
        changeItems.each { changeItem ->
            if (!latest || latest.created < changeItem.created) {
                latest = changeItem
            }
        }
        return latest?.created
    }

    static Timestamp getTimestampOfLastStatusChangeToSpecificStatus(Issue issue, String statusName) {
        List<ChangeItemBean> statusHistory = changeHistoryManager.getChangeItemsForField(issue, "status")

        Timestamp changeDate = null
        statusHistory.each { changeItemBean ->
            if (changeItemBean.toString == statusName)
                changeDate = changeItemBean.created
        }

        return changeDate
    }
}